package ru.ilnarsoultanov.chibbis.app.ui.restaurants

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.ilnarsoultanov.chibbis.app.models.RestaurantUIState
import ru.ilnarsoultanov.chibbis.app.models.mappers.RestaurantsToRestaurantsUIModel
import ru.ilnarsoultanov.chibbis.domain.usecase.RestaurantsInteractor

class RestaurantsViewModel(
    private val restaurantsInteractor: RestaurantsInteractor,
    private val restaurantsToRestaurantsUIModel: RestaurantsToRestaurantsUIModel
) : ViewModel() {
    private var disposable: Disposable? = null
    private val _restaurantsState = MutableLiveData<RestaurantUIState>()
    val restaurantsState: LiveData<RestaurantUIState> = _restaurantsState

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }

    init {
        loadRestaurants()
    }

    private fun loadRestaurants() {
        disposable?.dispose()
        disposable = restaurantsInteractor.getRestaurants()
            .observeOn(Schedulers.computation())
            .map { restaurantsToRestaurantsUIModel.transform(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _restaurantsState.value = RestaurantUIState.Loading }
            .subscribe({
                _restaurantsState.value = RestaurantUIState.Data(it)
            }, {
                _restaurantsState.value = RestaurantUIState.Error(it.toString())
            })
    }

    fun refreshRequested() {
        loadRestaurants()
    }
}