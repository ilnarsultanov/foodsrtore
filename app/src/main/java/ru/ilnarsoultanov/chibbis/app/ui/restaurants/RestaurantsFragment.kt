package ru.ilnarsoultanov.chibbis.app.ui.restaurants

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_restaurant.*
import ru.ilnarsoultanov.chibbis.R
import ru.ilnarsoultanov.chibbis.app.models.RestaurantUIState
import ru.ilnarsoultanov.chibbis.app.models.mappers.RestaurantsToRestaurantsUIModel
import ru.ilnarsoultanov.chibbis.app.ui.base.HomeViewModelFactory
import ru.ilnarsoultanov.chibbis.app.ui.restaurants.adapter.RestaurantsAdapter
import ru.ilnarsoultanov.chibbis.app.ui.reviews.ReviewsViewModel
import ru.ilnarsoultanov.chibbis.data.models.mappers.RestaurantsResponseToRestaurants
import ru.ilnarsoultanov.chibbis.data.network.API
import ru.ilnarsoultanov.chibbis.data.repository.NetworkRestaurantRepository
import ru.ilnarsoultanov.chibbis.domain.usecase.RestaurantsInteractor

class RestaurantsFragment : Fragment() {

    private lateinit var restaurantsViewModel: RestaurantsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val interactor = RestaurantsInteractor(
            NetworkRestaurantRepository(
                API.getApi(),
                RestaurantsResponseToRestaurants()
            )
        )

        restaurantsViewModel = ViewModelProvider(
            this,
            HomeViewModelFactory(
                restaurantsInteractor = interactor,
                restaurantsToRestaurantsUIModel = RestaurantsToRestaurantsUIModel()
            )
        ).get(RestaurantsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_restaurant, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        restaurantsRecyclerView.adapter = RestaurantsAdapter()

        swipeRefreshLayout.setOnRefreshListener {
            restaurantsViewModel.refreshRequested()
        }

        restaurantsViewModel.restaurantsState.observe(viewLifecycleOwner, Observer {
            swipeRefreshLayout.isRefreshing = it is RestaurantUIState.Loading

            when (it) {
                is RestaurantUIState.Data -> {
                    (restaurantsRecyclerView.adapter as RestaurantsAdapter).changeData(it.data)
                }
                is RestaurantUIState.None -> {
                    (restaurantsRecyclerView.adapter as RestaurantsAdapter).clear()
                }
                is RestaurantUIState.Error -> {
                    Snackbar.make(restaurantsRecyclerView, it.error, Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }
}
