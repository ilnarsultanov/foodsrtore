package ru.ilnarsoultanov.chibbis.app.ui.restaurants.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.ilnarsoultanov.chibbis.R
import ru.ilnarsoultanov.chibbis.app.models.RestaurantUIModel

class RestaurantsAdapter(private val data: MutableList<RestaurantUIModel> = mutableListOf()) :
    RecyclerView.Adapter<RestaurantHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantHolder {
        return RestaurantHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant, parent, false)
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RestaurantHolder, position: Int) {
        val item = data[position]


        Glide.with(holder.itemView.context).load(item.photoUrl)
            .placeholder(R.drawable.restaurant_photo_placeholder).into(holder.photoView)

        holder.nameView.text = item.name
        holder.specializationsView.text = item.specializations
        holder.positiveReviewsView.text = item.positiveReviews.toString()
    }

    fun changeData(newData: List<RestaurantUIModel>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }
}