package ru.ilnarsoultanov.chibbis.app.models.mappers

import ru.ilnarsoultanov.chibbis.app.models.RestaurantUIModel
import ru.ilnarsoultanov.chibbis.domain.models.Mapper
import ru.ilnarsoultanov.chibbis.domain.models.Restaurant

class RestaurantsToRestaurantsUIModel: Mapper<List<Restaurant>, List<RestaurantUIModel>> {
    override fun transform(value: List<Restaurant>): List<RestaurantUIModel> {
        return value.map {
            RestaurantUIModel(
                photoUrl = it.logo,
                name = it.name,
                specializations = it.specializations?.joinToString("/"),
                positiveReviews = it.positiveReviews
            )
        }
    }
}