package ru.ilnarsoultanov.chibbis.app.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.ilnarsoultanov.chibbis.app.models.mappers.RestaurantsToRestaurantsUIModel
import ru.ilnarsoultanov.chibbis.app.ui.restaurants.RestaurantsViewModel
import ru.ilnarsoultanov.chibbis.domain.usecase.RestaurantsInteractor

@Suppress("UNCHECKED_CAST")
class HomeViewModelFactory(
    private val restaurantsInteractor: RestaurantsInteractor,
    private val restaurantsToRestaurantsUIModel: RestaurantsToRestaurantsUIModel
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(RestaurantsViewModel::class.java)) {
            RestaurantsViewModel(
                restaurantsInteractor = restaurantsInteractor,
                restaurantsToRestaurantsUIModel = restaurantsToRestaurantsUIModel
            ) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}