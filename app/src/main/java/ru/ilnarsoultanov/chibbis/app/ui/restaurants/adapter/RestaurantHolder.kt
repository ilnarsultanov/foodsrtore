package ru.ilnarsoultanov.chibbis.app.ui.restaurants.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.ilnarsoultanov.chibbis.R

class RestaurantHolder(view: View): RecyclerView.ViewHolder(view) {
    val photoView: ImageView = itemView.findViewById(R.id.restaurantPhotoImg)
    val nameView: TextView = itemView.findViewById(R.id.restaurantNameTxt)
    val specializationsView: TextView = itemView.findViewById(R.id.specializationsTxt)
    val positiveReviewsView: TextView = itemView.findViewById(R.id.restaurantPositiveReviewsTxt)
}