package ru.ilnarsoultanov.chibbis.app.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class SplashViewModel : ViewModel() {
    private val _launchState = MutableLiveData<Unit>()
    val launchState: LiveData<Unit> get() = _launchState

    private var disposable: Disposable? = null

    init {
        disposable = Completable.timer(5, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                _launchState.value = Unit
            }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}