package ru.ilnarsoultanov.chibbis.app.ui.reviews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import ru.ilnarsoultanov.chibbis.R

class ReviewsFragment : Fragment() {

    private lateinit var reviewsViewModel: ReviewsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        reviewsViewModel = ViewModelProvider(this).get(ReviewsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_reviews, container, false)
        val textView: TextView = root.findViewById(R.id.text_notifications)
        reviewsViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}
