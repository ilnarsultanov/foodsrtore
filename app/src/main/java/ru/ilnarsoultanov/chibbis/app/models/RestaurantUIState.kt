package ru.ilnarsoultanov.chibbis.app.models

sealed class RestaurantUIState {
    object Loading: RestaurantUIState()
    object None: RestaurantUIState()
    class Error(val error: String): RestaurantUIState()
    class Data(val data: List<RestaurantUIModel>): RestaurantUIState()
}

data class RestaurantUIModel(
    val photoUrl: String?,
    val name: String,
    val specializations: String?,
    val positiveReviews: Int
)