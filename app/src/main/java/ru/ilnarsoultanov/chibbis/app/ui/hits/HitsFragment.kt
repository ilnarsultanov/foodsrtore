package ru.ilnarsoultanov.chibbis.app.ui.hits

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import ru.ilnarsoultanov.chibbis.R

class HitsFragment : Fragment() {

    private lateinit var hitsViewModel: HitsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        hitsViewModel =
            ViewModelProvider(this).get(HitsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_hits, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)
        hitsViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}
