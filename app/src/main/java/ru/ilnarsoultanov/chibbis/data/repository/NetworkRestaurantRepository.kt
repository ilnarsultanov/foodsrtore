package ru.ilnarsoultanov.chibbis.data.repository

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.ilnarsoultanov.chibbis.data.models.mappers.RestaurantsResponseToRestaurants
import ru.ilnarsoultanov.chibbis.data.network.API
import ru.ilnarsoultanov.chibbis.domain.models.Restaurant
import ru.ilnarsoultanov.chibbis.domain.repository.RestaurantRepository

class NetworkRestaurantRepository(
    private val api: API,
    private val restaurantsResponseToRestaurants: RestaurantsResponseToRestaurants
) : RestaurantRepository {
    override fun getRestautants(): Single<List<Restaurant>> {
        return api.getRestaurants()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .map { restaurantsResponseToRestaurants.transform(it) }
            .observeOn(AndroidSchedulers.mainThread())
    }
}