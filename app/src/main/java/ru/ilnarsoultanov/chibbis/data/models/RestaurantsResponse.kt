package ru.ilnarsoultanov.chibbis.data.models

import com.google.gson.annotations.SerializedName

data class RestaurantsResponse(
	@field:SerializedName("DeliveryTime")
	val deliveryTime: Int,

	@field:SerializedName("MinCost")
	val minCost: Int,

	@field:SerializedName("Specializations")
	val specializations: List<SpecializationsItem>? = null,

	@field:SerializedName("DeliveryCost")
	val deliveryCost: Int,

	@field:SerializedName("PositiveReviews")
	val positiveReviews: Int,

	@field:SerializedName("Name")
	val name: String,

	@field:SerializedName("Logo")
	val logo: String? = null,

	@field:SerializedName("ReviewsCount")
	val reviewsCount: Int
)

data class SpecializationsItem(
	@field:SerializedName("Name")
	val name: String
)