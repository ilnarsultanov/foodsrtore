package ru.ilnarsoultanov.chibbis.data.models.mappers

import ru.ilnarsoultanov.chibbis.data.models.RestaurantsResponse
import ru.ilnarsoultanov.chibbis.domain.models.Mapper
import ru.ilnarsoultanov.chibbis.domain.models.Restaurant

class RestaurantsResponseToRestaurants : Mapper<List<RestaurantsResponse>, List<Restaurant>> {
    override fun transform(value: List<RestaurantsResponse>): List<Restaurant> {
        return value.map {
            Restaurant(
                deliveryTime = it.deliveryTime,
                minCost = it.minCost,
                specializations = it.specializations?.map { spec-> spec.name }?.toSet(),
                deliveryCost = it.deliveryCost,
                positiveReviews = it.positiveReviews,
                name = it.name,
                logo = it.logo,
                reviewsCount = it.reviewsCount
            )
        }
    }
}