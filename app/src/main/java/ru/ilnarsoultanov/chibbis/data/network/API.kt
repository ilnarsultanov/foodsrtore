package ru.ilnarsoultanov.chibbis.data.network

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import ru.ilnarsoultanov.chibbis.data.models.RestaurantsResponse

interface API {
    @GET("restaurants")
    fun getRestaurants() : Single<List<RestaurantsResponse>>

    companion object {
        fun getApi(): API {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://front-task.chibbistest.ru/api/v1/")
                .build()

            return retrofit.create(API::class.java)
        }
    }
}