package ru.ilnarsoultanov.chibbis.domain.models

interface Mapper<I, O> {
    fun transform(value: I): O
}