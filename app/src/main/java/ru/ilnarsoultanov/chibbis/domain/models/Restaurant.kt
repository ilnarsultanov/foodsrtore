package ru.ilnarsoultanov.chibbis.domain.models

data class Restaurant(
    val deliveryTime: Int,
    val minCost: Int,
    val specializations: Set<String>? = null,
    val deliveryCost: Int,
    val positiveReviews: Int,
    val name: String,
    val logo: String? = null,
    val reviewsCount: Int
)