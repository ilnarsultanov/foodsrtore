package ru.ilnarsoultanov.chibbis.domain.repository

import io.reactivex.Single
import ru.ilnarsoultanov.chibbis.domain.models.Restaurant

interface RestaurantRepository {
    fun getRestautants(): Single<List<Restaurant>>
}