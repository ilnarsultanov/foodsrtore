package ru.ilnarsoultanov.chibbis.domain.usecase

import ru.ilnarsoultanov.chibbis.domain.repository.RestaurantRepository

class RestaurantsInteractor(
    private val restaurantRepository: RestaurantRepository
) {

    fun getRestaurants() = restaurantRepository.getRestautants()
}